package com.example.demodbfirst.services;

import com.example.demodbfirst.entities.Customer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CustomerService {

    Customer create(Customer customer);

    Optional<Customer> readOne(long id);

    List<Customer> readAll();

    void delete(long id);

    List<Customer> getAllCustomerSortedByLastName();

    Customer updateFirstNameAndLastName(long customerId,String firstName,String lastName);

}

